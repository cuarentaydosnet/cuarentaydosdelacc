const requestJson = require('request-json');

exports.handler = function(event,context,callback){
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    const mitoken = event.stageVariables.tokenA1;
    const api = event.stageVariables.api
    console.log(event)
    
    // comporbamos el saldo
    var cliente = requestJson.createClient(api)
    cliente.headers['Authorization'] = mitoken;
    cliente.headers['Content-Type'] = 'application/json';


    var consulta ="cuentas/" + event.pathParameters.accountid + "/saldo"
    cliente.get(consulta, function(err,res,body){
        if(err){
            var response = {
                "statusCode" : 501,
                "headers" : {"Access-Control-Allow-Origin": "*"}, 
                "body": JSON.stringify(err)
            };
        } else {
            var saldo=body.saldo;        
            var consulta ="cuentas/" + event.pathParameters.accountid
            cliente.get(consulta, function(err,res,body){
                if(err){
                    var response = {
                        "statusCode" : 501,
                        "headers" : {"Access-Control-Allow-Origin": "*"}, 
                        "body": JSON.stringify(err)
                    };
                } else {
                    var tipo=body[0].tipo;
                    if(tipo==0 && saldo !=0){
                        var response = { 
                            "statusCode": 401,
                            "headers" : {"Access-Control-Allow-Origin": "*"},  
                            "body": '{"msg":"No se puede eleiminar una cuenta con saldo."}',
                            "isBase64Encoded": false
                        };
                        return callback(null,response); 
                    } else if (tipo == 1 && saldo != 3000 ){
                        var response = { 
                            "statusCode": 401,
                            "headers" : {"Access-Control-Allow-Origin": "*"},  
                            "body": '{"msg":"No se puede eleiminar una tarjeta con crédito."}',
                            "isBase64Encoded": false
                        };
                        return callback(null,response); 
                    } else {
                        // eliminamos la cuenta
                        var query = 'account/' + event.pathParameters.accountid + "?" + apikey
                        var httpClient = requestJson.createClient(db);
                        httpClient.del(query,function(err,resMLab,body){
                            if(err){
                                var response = {
                                    "statusCode" : 501,
                                    "headers" : {"Access-Control-Allow-Origin": "*"}, 
                                    "body": JSON.stringify(err)
                                }
                            } else {
                                var response = { 
                                    "statusCode": 200,
                                    "headers" : {"Access-Control-Allow-Origin": "*"},
                                    "body": "{\"msg\" : \"Cuenta eliminada\"}",
                                    "isBase64Encoded": false
                                };
                            }
                            return callback(null,response);
                        })
                    }
                }
            })
        }
    })
}